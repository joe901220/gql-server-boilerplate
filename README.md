# gql-server-boilerplate

graphql, typescript를 이용한 boilerplate

---

## Getting Install

Install it using [yarn](https://yarnpkg.com/lang/en/)

```
yarn
yarn start
```

open http://localhost:3000/

---

## Skill Stack:

#### \* ) graphql-yoga 기반

#### 1) Language: Graphql, Typescript, Javascript

#### 2) Data: MySQL

#### 3) Utility: Storybook

#### 4) Test: Jest, Enzyme

## Getting Start

#### 1) prisma 연결 필요.

```
prisma login
prisma init
```

#### 2) prisma 데모 서버 연결(다른 db, 혹은 존재하는 db 연결시)

[prisma start](https://www.prisma.io/docs/get-started/01-setting-up-prisma-existing-database-JAVASCRIPT-a001/#set-up-prisma) 참조

#### 3) prisma 연결 후, datamodel.prisma, models.graphql 내용 수정 후

```
 yarn prisma
```

## main files:

#### 1) src -> bin -> www.ts [start point],

#### 2) src -> lib -> server.ts [set middlewares and server]

#### 3) src -> lib -> schema.ts [모든 gql, resolver파일 통합]
